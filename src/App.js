import React from "react";
import { Navbar, Nav, NavItem, NavLink } from "reactstrap";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./Screens/Home";
import Profile from "./Screens/Profile";
import ContactUs from "./Screens/ContactUs";
import Blog from "./Screens/Blog";

export default function App() {
  return (
    <Router>
      <div className="navigation">
        <Navbar color="light" light expand="md">
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/profile">Profile</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/contact-us">Contact Us</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/blog">Blog</NavLink>
            </NavItem>
          </Nav>
          {/* <NavbarText>Abracadabra.edu</NavbarText> */}
        </Navbar>

        {/* <Link to="/">Home</Link>
        <Link to="/profile">Profile</Link>
        <Link to="/contact-us">Contact Us</Link>
        <Link to="/blog">Blog</Link> */}
      </div>

      <Switch>
        <Route component={Home} path="/" exact={true} />
        <Route component={Profile} path="/profile" />
        <Route component={ContactUs} path="/contact-us" />
        <Route component={Blog} path="/blog" />
      </Switch>
    </Router>
  );
}
