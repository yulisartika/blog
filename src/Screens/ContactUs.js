import React, { Component } from "react";
import { Button } from "reactstrap";

export default class ContactUs extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      message: "",
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();
    alert(
      `Hi ${this.state.name}, thank you for contacting us! We will reach you soon in no time`
    );
  };

  handleChange = (key, value) => {
    switch (key) {
      case "name":
        this.setState({ name: value });
        break;
      case "email":
        this.setState({ email: value });
        break;
      case "message":
        this.setState({ message: value });
        break;
      default:
        break;
    }
  };

  render() {
    return (
      <form className="form-container" onSubmit={this.handleSubmit}>
        <table>
          <tr>
            <td>Full Name</td>
            <td>
              <input
                type="text"
                placeholder="full name"
                onChange={(event) =>
                  this.handleChange("name", event.target.value)
                }
              />
            </td>
          </tr>
          <tr>
            <td>Email</td>
            <td>
              <input
                type="text"
                placeholder="email"
                onChange={(event) =>
                  this.handleChange("email", event.target.value)
                }
              />
            </td>
          </tr>
          <tr>
            <td>Message</td>
            <td>
              <input
                type="text"
                placeholder="short message"
                onChange={(event) =>
                  this.handleChange("message", event.target.value)
                }
              />
            </td>
            <td>
              <Button color="secondary" type="submit">
                Send
              </Button>
            </td>
          </tr>
        </table>
      </form>
    );
  }
}
